__author__ = 'Olivier Van Cutsem'

from ems_config import EMS_COMFORT_temperature,EMS_COMFORT_luminosity, EMS_COMFORT_humidity, EMS_COMFORT_presence, EMS_COMFORT_irradiance, \
    EMS_ENERGY_soc, EMS_ENERGY_power, EMS_ENERGY_nb_cycle

# LIST OF EMS KEY FIELDS CONVERTED FOR INTERNAL FROM BMS DATA

# Names
DB_KEY_UNIT_NAME = 'unit_name'
DB_KEY_ROOM_NAME = 'room_name'

# IDs
DB_KEY_UNIT_ID = 'unit_id'
DB_KEY_ROOM_ID = 'room_id'

# Geometric data
DB_KEY_ROOM_SURFACE = 'room_surface'
DB_KEY_ROOM_VOLUME = 'room_volume'
DB_KEY_ROOM_NATURE = 'nature'
DB_KEY_ROOM_TYPE = 'type'

# COMFORT QUANTITY & CONSTRAINTS
DB_KEY_COMFORT_TYPE = 'COMFORT_TYPE'
DB_KEY_COMFORT_CONSTRAINTS = 'COMFORT_CONSTRAINTS'
DB_KEY_COMFORT_VALUE = 'COMFORT_VALUE'
DB_KEY_EMS_SENSORS_CONVERSION = {"TEMP": EMS_COMFORT_temperature,
                                 "TEM": EMS_COMFORT_temperature,
                                  "LUM": EMS_COMFORT_luminosity,
                                  "PRE": EMS_COMFORT_presence,
                                  "HUM": EMS_COMFORT_humidity,
                                  "IRR": EMS_COMFORT_irradiance, "IRR_BEAM": EMS_COMFORT_irradiance,
                                  "BAT": EMS_ENERGY_soc,
                                  "SOC": EMS_ENERGY_soc,
                                  "P": EMS_ENERGY_power,
                                  "CYCLES": EMS_ENERGY_nb_cycle}  # from BMS language to EMS

DB_KEY_ENERGY_MEASURE_TYPE = 'energy_type_of_measure'
DB_KEY_ENERGY_MEASURE_VALUE = 'energy_measure_value'
DB_KEY_ENERGY_ENTITY_RELATED_TYPE = 'energy_measure_type'
DB_KEY_ENERGY_ENTITY_RELATED_ID = 'energy_entity_id'
DB_KEY_ENERGY_RELATED_SENSOR_ID = 'energy_sensor_id'

    # time-varying Constraints

DB_KEY_COMFORT_CONSTRAINTS = 'constraints'
DB_KEY_COMFORT_TIMECONSTRAINTS_TIMEVECTOR = 'comfort_timevector'
DB_KEY_COMFORT_TIMECONSTRAINTS_MINVECTOR = 'comfort_minvector'
DB_KEY_COMFORT_TIMECONSTRAINTS_MAXVECTOR = 'comfort_maxvector'

# Reference value
DB_KEY_COMFORT_REFERENCE_VALUE = 'ref_value'

# ACTUATOR

DB_KEY_ACTUATOR_ACTION_TYPE = 'actuator_action_type'
DB_KEY_ACTUATOR_NAME = 'actuator_name'
DB_KEY_ACTUATOR_STATUS = 'actuator_status'
DB_KEY_ENERGY_RELATED_ACTUATOR_ID = 'energy_sensor_id'
DB_KEY_ACTUATOR_ADD_PARAM = 'add_parameters'
DB_KEY_ACTUATOR_LP = 'lp_activity_actuator'

# Loads

DB_KEY_LOAD_NAME = 'load_name'
DB_KEY_LOAD_LP = 'load_profile'
DB_KEY_LOAD_CONTROL_TYPE = 'load_control_type'
DB_KEY_LOAD_CURRENT_VALUE = 'load_current_value'
DB_KEY_LOAD_DEVICE_TYPE = 'load_device_type'
DB_KEY_LOAD_MODEL_PARAM = 'load_model_param'

# Storage
DB_KEY_STORAGE_TYPE = 'storage_type'
DB_KEY_STORAGE_NAME = 'storage_name'
DB_KEY_STORAGE_CAPA = 'storage_capcity'
DB_KEY_STORAGE_LIFE = 'storage_life_cycle'
DB_KEY_STORAGE_EFFICIENCY = 'storage_efficiency'
DB_KEY_STORAGE_MODEL = 'storage_model_parameters'
DB_KEY_STORAGE_CURRENT_CYCLE = 'storage_current_cycle'
DB_KEY_STORAGE_CURRENT_POWER = 'storage_current_power'
DB_KEY_STORAGE_CURRENT_SOC = 'storage_current_soc'
DB_KEY_STORAGE_MIN_SOC = 'storage_min_soc'
DB_KEY_STORAGE_MAX_SOC = 'storage_max_soc'

DB_KEY_PHEV_ID = 'phev_id'
DB_KEY_PHEV_SCHEDULE_ARRIVE_TIME = 'phev_schedule_arrive'
DB_KEY_PHEV_SCHEDULE_LEAVE_TIME = 'phev_schedule_leave'
DB_KEY_PHEV_ALLOW_DISCHARGE = 'phev_disch_allow'

# Generation

DB_KEY_GENERATION_NAME = 'generation_name'
DB_KEY_GENERATION_TYPE = 'generation_type'
DB_KEY_GENERATION_NOM_POWER = 'generation_nominal_power'
DB_KEY_GENERATION_CURRENT_POWER = 'generation_current_power'
DB_KEY_GENERATION_MODEL_PARAM = 'generation_model_parameters'

# Interfaces

DB_KEY_INTERFACE_TYPE_KEY = 'interface_type'
DB_KEY_INTERFACE_WALL_KEY = 'interface_wall'
DB_KEY_INTERFACE_WINDOW_KEY = 'interface_window'
DB_KEY_INTERFACE_DOOR_KEY = 'interface_door'

DB_KEY_INTERFACE_SURFACE = 'interface_surface'
DB_KEY_INTERFACE_THICKNESS = 'interface_thickness'
DB_KEY_INTERFACE_THERMAL_PARAM = 'interface_thermal_prop'
DB_KEY_INTERFACE_OPENING_ACTUATOR = 'interface_window_opening_actuator_id'

    # Windows
DB_KEY_INTERFACE_WINDOW_BLIND_ACTUATOR = 'interface_window_blind_actuator_id'


