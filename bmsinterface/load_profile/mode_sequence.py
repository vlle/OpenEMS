
import numpy
from stat_distr import statDistribution

# Modes
LOAD_PROFILE_SEQ_LABEL = 'seq_label'
LOAD_PROFILE_SEQ_MODE = 'seq_mode'
LOAD_PROFILE_SEQ_DUR_TYPE = 'seq_dur_type'
LOAD_PROFILE_SEQ_DUR_PARAM = 'seq_dur_param'


class LPSequence(object):
    """
    Defines a deterministic sequence of modes within a load profile state
    """

    def __init__(self, seq_descr):

        self.__list_atoms = []
        self.__name_mapping = dict()

        assert(type(seq_descr) is list)

        i = 0
        for atom_seq in seq_descr:
            assert(type(atom_seq) is dict)
            assert(v in atom_seq for v in [LOAD_PROFILE_SEQ_MODE, LOAD_PROFILE_SEQ_DUR_TYPE, LOAD_PROFILE_SEQ_DUR_PARAM])

            label = atom_seq[LOAD_PROFILE_SEQ_LABEL]
            self.__name_mapping[atom_seq[LOAD_PROFILE_SEQ_LABEL]] = i

            mode = atom_seq[LOAD_PROFILE_SEQ_MODE]
            type_dur_distr = atom_seq[LOAD_PROFILE_SEQ_DUR_TYPE]
            param_dur_distr = atom_seq[LOAD_PROFILE_SEQ_DUR_PARAM]
            dur_distr = statDistribution(type_dur_distr, param_dur_distr)

            self.__list_atoms.append(LPAtomSequence(mode, label, dur_distr))
            i += 1

    def next_mode(self, current_mode):

        if current_mode == len(self.__list_atoms) - 1:  # last item in the sequence
            return None
        else:
            return current_mode + 1

    def get(self, idx):
        if idx < len(self.__list_atoms):
            return self.__list_atoms[idx]
        else:
            return None

    def get_id(self, label):
        return self.__name_mapping[label]

    def sequence_duration(self):
        """
        Get the mean total duration
        :return: a float representing the total sequence duration
        """
        return sum([x.duration.equivalent_quantity for x in self.__list_atoms])

    def get_starting_mode(self):
        """
        Get the starting mode of the sequence
        :return: an integer
        """
        return 0


class LPSequenceStochastique(LPSequence):
    """
    Defines a stochastique sequence of modes within a load profile state
    """

    def __init__(self, seq_descr, seq_transition):
        super(LPSequenceStochastique, self).__init__(seq_descr)

        # Transition matrix
        self.__transitions = seq_transition

        # In the transition matrix, the diagonal indicates starting proba
        nb_modes = len(self.__transitions[self.get(0).label].keys())
        self.__starting_proba = nb_modes * [0]
        for i in range(nb_modes):
            lab_i = self.get(i).label
            self.__starting_proba[i] = self.__transitions[lab_i][lab_i]
            self.__transitions[lab_i][lab_i] = 0  # no transition from this mode to this mode

    def next_mode(self, current_mode):

        current_mode_label = self.get(current_mode).label
        vect_trans = self.__transitions[current_mode_label]

        rand_nb = numpy.random.random_sample()
        acc = 0.0
        for mode_label, proba in vect_trans.items():
            if rand_nb < acc+proba:
                return self.get_id(mode_label)
            else:
                acc += proba

    def sequence_duration(self):
        return float('inf')

    def get_starting_mode(self):
        """
        Get the starting mode of the sequence
        :return: an integer
        """

        return numpy.random.choice(numpy.arange(0, len(self.__starting_proba)), p=self.__starting_proba)

class LPAtomSequence:
    """
    Defines an atom of a LP-sequence
    """

    def __init__(self, mode, label, duration_descr):
        """
        :param mode:
        :param transition:
        """

        # The mode name of this atom sequence
        self.__mode = mode

        # The label given to this atom sequence
        self.__label = label

        # The duration of this atom sequence
        self.__dur = duration_descr

    @property
    def mode(self):
        return self.__mode

    @property
    def label(self):
        return self.__label

    @property
    def duration(self):
        return self.__dur
