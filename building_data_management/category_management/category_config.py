__author__ = 'Olivier Van Cutsem'

from building_data_management.category_management.cat_tree import DICT_CREATION_LABEL_LIST

### THE MAIN CATEGORIES OF ENERGY-RELATED ENTITIES
EMS_CATEGORY_ENTITY_LOAD = 'L'
EMS_CATEGORY_ENTITY_STORAGE = 'S'
EMS_CATEGORY_ENTITY_GENERATION = 'G'

# Level 'L' entities types
EMS_CATEGORY_ENTITY_LOAD_USER_DRIVEN = 'USER-DRIVEN'
EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE = 'DEF'
EMS_CATEGORY_ENTITY_LOAD_THERM = 'THERM'

# Level 'S' entities types
EMS_CATEGORY_ENTITY_STORAGE_FIXED = 'FIXED'
EMS_CATEGORY_ENTITY_STORAGE_PHEV = 'PHEV'

### THEIR CORRESPONDING CLASS CONTAINING DATA STRUCTURE
EMS_CATEGORY_ENTITY_DATASTRUCTURE_CLASSNAMES = {EMS_CATEGORY_ENTITY_LOAD:
                                                    {EMS_CATEGORY_ENTITY_LOAD_USER_DRIVEN: 'BuildingLoadData',
                                                     EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE: 'BuildingShiftLoadData',
                                                     EMS_CATEGORY_ENTITY_LOAD_THERM: 'BuildingThermalLoadData'},
                                                EMS_CATEGORY_ENTITY_STORAGE:
                                                    {EMS_CATEGORY_ENTITY_STORAGE_FIXED: 'BuildingStorageData',
                                                     EMS_CATEGORY_ENTITY_STORAGE_PHEV: 'BuildingPHEVData'},
                                                EMS_CATEGORY_ENTITY_GENERATION: 'BuildingGenerationData'}

EMS_CATEGORY_ENTITY_DATASTRUCTURE_MODULES = {EMS_CATEGORY_ENTITY_LOAD:
                                                    {EMS_CATEGORY_ENTITY_LOAD_USER_DRIVEN: 'load_data',
                                                     EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE: 'load_data',
                                                     EMS_CATEGORY_ENTITY_LOAD_THERM: 'load_data'},
                                                EMS_CATEGORY_ENTITY_STORAGE:
                                                    {EMS_CATEGORY_ENTITY_STORAGE_FIXED: 'storage_data',
                                                     EMS_CATEGORY_ENTITY_STORAGE_PHEV: 'phev'},
                                                EMS_CATEGORY_ENTITY_GENERATION: 'generation_data'}

# Level 1 lists
ems_cat_main_list = [EMS_CATEGORY_ENTITY_LOAD, EMS_CATEGORY_ENTITY_STORAGE, EMS_CATEGORY_ENTITY_GENERATION]

# Level 2 lists for 'L'
ems_cat_load_list = [EMS_CATEGORY_ENTITY_LOAD_USER_DRIVEN,
                     EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE,
                     EMS_CATEGORY_ENTITY_LOAD_THERM]

# Level 2 lists for 'S'
ems_cat_stor_list = [EMS_CATEGORY_ENTITY_STORAGE_FIXED,
                     EMS_CATEGORY_ENTITY_STORAGE_PHEV]

### The structure for EMS categories initialization
EMS_CATEGORY_STRUCTURE = {DICT_CREATION_LABEL_LIST: ems_cat_main_list,
                          EMS_CATEGORY_ENTITY_LOAD: {DICT_CREATION_LABEL_LIST: ems_cat_load_list},
                          EMS_CATEGORY_ENTITY_STORAGE: {DICT_CREATION_LABEL_LIST: ems_cat_stor_list}
                          }
