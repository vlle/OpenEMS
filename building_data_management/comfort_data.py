__author__ = 'Olivier Van Cutsem'

from ems_config import *

class BuildingComfortData():

    """
    BuildingComfortData represents a data structure that hold information about a comfort in a zone (typically, a room)
    A comfort is characterized mainly by its type and value. It might also be subject to constraints.
    It is generally linked to sensor/actuator (defined by an ID) that update/change its value.
    It might also be linked to a load whose operation influences its value.
    In some cases, a forecast may be available.
    """
    def __init__(self, type_quant, cons=None, init_val=None):

        # Type of comfort quantity
        self.__comfort_type = type_quant

        # The current value of the comfort quantity
        self.__current_value = init_val

        # The constraints linked to this comfort quantity
        self.__comfort_constraints = cons

        # The sensor, actuator & load linked to this comfort
        self.__linked_sensor = None
        self.__linked_actuator = None
        self.__linked_loads = None

        # The constraints linked to this comfort quantity
        self.__comfort_forecast = None

        # The ideal value of the comfort quantity
        self.__ref_value = None

    @property
    def comfort_value(self):
        """
        Get the current value of the comfort
        :return: a numerical value representing the comfort value
        """
        return self.__current_value

    @comfort_value.setter
    def comfort_value(self, v):
        """
        Set the current value of the comfort
        :v: a numerical value representing the comfort value
        """
        self.__current_value = v

    @property
    def ref_value(self):
        """
        Get the reference value of the comfort
        :return: a numerical value representing the comfort reference value
        """
        return self.__ref_value

    @ref_value.setter
    def ref_value(self, v):
        """
        Set the reference value of the comfort
        :v: a numerical value representing the comfort reference value
        """
        self.__ref_value = v

    @property
    def comfort_constraints(self):
        """
        Get the constraints on the comfort
        :return: a Constraint object (or a subclass) representing the constraints
        """
        return self.__comfort_constraints

    @comfort_constraints.setter
    def comfort_constraints(self, v):
        """
        Set the constraints on the comfort
        :v: a Constraint object (or a subclass) representing the constraints
        """
        self.__comfort_constraints = v

    @property
    def sensor(self):
        """
        Get the sensor linked to this comfort
        :return: an integer value representing the sensor ID
        """
        return self.__linked_sensor

    @sensor.setter
    def sensor(self, s):
        """
        Set the sensor linked to this comfort
        :s: an integer value representing the sensor ID
        """
        self.__linked_sensor = s

    @property
    def actuator(self):
        """
        Get the actuator linked to this comfort
        :return: an integer value representing the actuator ID
        """
        return self.__linked_actuator

    @actuator.setter
    def actuator(self, a):
        """
        Set the actuator linked to this comfort
        :s: an integer value representing the actuator ID
        """
        self.__linked_actuator = a

    @property
    def loads(self):
        """
        Get the load that influences this comfort
        :return: an integer value representing the load ID
        """
        return self.__linked_loads

    @loads.setter
    def loads(self, l):
        """
        Set the load that influences this comfort
        :l: an integer value representing the load ID
        """
        self.__linked_loads = l

    @property
    def forecast(self):
        """
        Get the comfort forecast
        :return: a PiecewiseConstantSignal representing the forecast values over time
        """
        return self.__comfort_forecast

    @forecast.setter
    def forecast(self, v):
        """
        Set the comfort forecast
        :v: a PiecewiseConstantSignal representing the forecast values over time
        """
        self.__comfort_forecast = v
