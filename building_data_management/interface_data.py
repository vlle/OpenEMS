__author__ = 'Olivier Van Cutsem'

from building_data_management.interface_management.generic_interfaces import *


class EquivalentInterface:

    """
    TODO
    """
    def __init__(self, l_interfaces):

        #Todo - idea: if a R is >> than the others, maybe the other R can be simplified & idem for C <<

        self.list_of_interface = l_interfaces

        self.__surface = 0
        for i in self.list_of_interface:
            self.__surface += i.surface

    @property
    def surface(self):
        return self.__surface

    @property
    def thermalProp(self):
        # TODO: make a RC equivalent of all the interfaces in the list

        max_obj = self.list_of_interface[0]
        for i in self.list_of_interface:
            if i.surface > max_obj.surface:
                max_obj = i

        return max_obj  # Intermediate solution: return the one with the largest interface

    def getLuminosityModel(self):
        # Combine the 3 interfaces for outputting an "equivalent" one
        pass
