__author__ = 'Olivier Van Cutsem'

from building_data_management.energyrelated_data import BuildingEnergyRelatedEntityData
from signal_and_constraint_management.signal_data import PiecewiseConstantSignal
from bmsinterface.load_profile.ve_lp import *

EMS_LOAD_DEVICE_TYPE_HP = 'Heat Pump'
EMS_LOAD_DEVICE_TYPE_AC = 'Air Conditioner'
EMS_LOAD_DEVICE_TYPE_ELEC_HEATER = 'Electrical Heater'
EMS_LOAD_DEVICE_MAP_ID = {EMS_LOAD_DEVICE_TYPE_HP: 16, EMS_LOAD_DEVICE_TYPE_AC: 17, EMS_LOAD_DEVICE_TYPE_ELEC_HEATER: 10}


class BuildingLoadData(BuildingEnergyRelatedEntityData):
    """
    A class that stores the data structure of any load
    """

    def __init__(self, name, lp, related_sensors, additional_param=None):
        """
        Constructor
        :param name: a string representing the name of the load
        :param lp: a LoadProfile object representing the load profile of the load
        :param related_sensors: a dictionary listing the sensors linked to this load
        """
        super(BuildingLoadData, self).__init__(related_sensors=related_sensors, name=name)

        self.__loadProfile = lp  #  This Load Profile structure contains all the characteristics about the power consumption

        self.__current_power = None  # The current power consumption of the load

        self.__additional_param = additional_param  # Additional parameter for modelling the load behavior

        self.__device_type = None  # The specific type of device (computer, lamp, electrical heater, heat pump, etc)

    @property
    def load_profile(self):
        """
        Get the load profile of this load
        :return: a LoadProfile object
        """
        return self.__loadProfile

    @load_profile.setter
    def load_profile(self, lp):
        """
        Set the load profile of this load
        :lp: a LoadProfile object
        """
        self.__loadProfile = lp


    @property
    def device_type(self):
        """
        Get the device type of this load
        :return: a string
        """
        return self.__device_type

    @device_type.setter
    def device_type(self, t):
        """
        Set the device type of this load
        :t: a string
        """
        self.__device_type = t

    @property
    def additional_param(self):
        return self.__additional_param

    def consumption_forecast(self, time_data):
        """
        Get the forecast of the load power consumption for the time interval time_data
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :return: a list of numerical values representing the power forecast, corresponding to time_data
        """
        (t_cur, t_hor, t_step) = time_data

        return (int(t_hor/t_step)) * [0]

        # Load Profile manipulation
        event_signal = PiecewiseConstantSignal([t_cur], 0)  # Init with [x0, 0] couple

        #TODO:
        # Go through the lp activities t_start, t_end until reaching t_cur, then build the event-based signal.
        act_list = self.load_profile.activity.seq_activities

        return event_signal.get_interval_val(t_cur, t_cur+t_hor, t_step)


class BuildingShiftLoadData(BuildingLoadData):
    """
    A class that store the data structure of any deferrable/shiftable load
    """
    def __init__(self, name, lp, related_sensors, additional_param=None):
        super(BuildingShiftLoadData, self).__init__(name=name, lp=lp, related_sensors=related_sensors, additional_param=additional_param)

        # The starting time defined by the EMS
        self.scheduled_time = None
        self.last_triggered_time = None

    def get_max_starting_time(self, state='ON'):
        return self.load_profile.get_max_starting_time(state)

    def get_min_starting_time(self):
        return self.load_profile.min_starting_time

    def get_duration(self, state='ON'):
        return self.load_profile.duration(state)

    def set_scheduled_time(self, t):
        self.scheduled_time = t

    def get_scheduled_time(self):
        return self.scheduled_time

    def reset_scheduled_time(self):
        self.set_scheduled_time(None)

    def set_triggered_time(self, t):
        self.last_triggered_time = t

    def is_running(self, t):

        return self.last_triggered_time is not None and (self.last_triggered_time <= t <= self.last_triggered_time + self.get_duration())

    def get_sampled_consumption(self, dt):
        """
        This method returns the consumption profile of the load, sampled at a specified time interval
        :param dt: the value of the time interval
        :return: an ordered list of values, containing the interpolated power consumption for the corresponding time points
        """

        event_vect = list()
        t_cur = 0
        for seq_atom in self.load_profile.get_iter(state='ON'):
            power = self.load_profile.get_mode(seq_atom.mode).power_distribution.equivalent_quantity
            duration = seq_atom.duration.equivalent_quantity
            event_vect.append((t_cur, power))
            t_cur += duration

        # Use the PiecewiseConstantSignal object in order to sample the event-based vector
        signal_obj = PiecewiseConstantSignal(list(x for x, y in event_vect), list(y for x, y in event_vect), t_cur)

        return signal_obj.get_interval_val(0, t_cur, dt)

    def consumption_forecast(self, time_data):
        """
        Overwrite the method from the generic one
        """

        (t_cur, t_hor, t_step) = time_data
        nb_pts = int(t_hor / t_step)
        ret = (nb_pts) * [0]

        # Not yet decided when to start it or already finished
        if self.scheduled_time is None or self.scheduled_time+self.get_duration() < t_cur:
            return ret

        sampled_profile = self.get_sampled_consumption(t_step)

        # The index in the forecast vector corresponding to the first profile value AND
        # the index in the profile of the first  value to take
        idx_forecast_start = max(int((self.scheduled_time-t_cur)/t_step), 0)
        idx_profile_start = max(int((t_cur-self.scheduled_time)/t_step), 0)

        idx_profile = idx_profile_start
        for i in range(idx_forecast_start, nb_pts):
            if idx_profile >= len(sampled_profile):
                break

            ret[i] = sampled_profile[idx_profile]
            idx_profile += 1

        return ret


THERMAL_LOAD_MODEL_COP = 'cop_param'
THERMAL_LOAD_MODEL_COP_NOMINAL = 'nominal_value'
THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF = 'linear_model'
THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_CST = 'cst_coeff'
THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_TOUT = 't_out_coeff'
THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_TIN = 't_in_coeff'


class BuildingThermalLoadData(BuildingLoadData):
    """
    A class that store the data structure of any thermal load
    """

    DEFAULT_THERMAL_EFFICIENCY = 0.95

    def __init__(self, name, lp, related_sensors, additional_param):
        super(BuildingThermalLoadData, self).__init__(name=name, lp=lp, related_sensors=related_sensors, additional_param=additional_param)

    @property
    def max_power(self):
        return self.load_profile.max_p

    @property
    def thermal_efficiency(self, t_param=None):
        """
        Return the modeled thermal efficiency, may be depending on the surrounding temperature "t_param"
        :param t_param: [OPTIONAL] a tuple (t_out, t_in) representing:
            - t_out
        :return: the thermal efficiency of the
        """

        if self.additional_param is None:
            return self.DEFAULT_THERMAL_EFFICIENCY

        # Nominal value of COP or COP linearly adapted
        if t_param is None:
            if THERMAL_LOAD_MODEL_COP not in self.additional_param.keys() or (THERMAL_LOAD_MODEL_COP_NOMINAL not in self.additional_param[THERMAL_LOAD_MODEL_COP].keys() and t_param is None):
                return self.DEFAULT_THERMAL_EFFICIENCY
            else:
                return self.additional_param[THERMAL_LOAD_MODEL_COP][THERMAL_LOAD_MODEL_COP_NOMINAL]
        else:  # The COP is a function of the surrounding conditions
            if THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF not in self.additional_param[THERMAL_LOAD_MODEL_COP].keys():
                return self.DEFAULT_THERMAL_EFFICIENCY
            else:
                (t_in, t_out) = t_param
                coeff_dict = self.additional_param[THERMAL_LOAD_MODEL_COP][THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF]
                a_0 = coeff_dict[THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_CST]
                a_tout = coeff_dict[THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_TOUT]
                a_tin = coeff_dict[THERMAL_LOAD_MODEL_COP_LINEAR_MODEL_COEFF_TIN]

                return a_0 + a_tin * t_in + a_tout * t_out
