__author__ = 'Olivier Van Cutsem'

from building_data_management.energyrelated_data import BuildingEnergyRelatedEntityData
from storage_data import BuildingStorageData
from bmsinterface.load_profile.stat_distr import statDistribution
import numpy


class BuildingPHEVData(BuildingStorageData):

    def __init__(self, name, capa, related_sensors):
        """
        Constructor
        :param name: a string representing the name of the Energy Storage System
        :param capa: a numerical value representing the capacity of the Energy Storage System
        :param schedule: a tuple (t_arrive, t_leave) of statDistribution objects representing the statistical distribution of arriving/leaving time instant
        :param related_sensors: a dictionary listing the sensors linked to this Energy Storage System
        """
        super(BuildingPHEVData, self).__init__(related_sensors=related_sensors, name=name, capa=capa)

        self.__schedule_constraints_arriving = None
        self.__schedule_constraints_leaving = None

        self.__discharge_allowed = False

    def set_schedule_constraints(self, sched_arrive, sched_leave):
        """
        Set the arriving and leaving time instant distribution
        :param sched_arrive: a tuple of (mean, std)
        :param sched_leave: a tuple of (mean, std)
        """
        self.__schedule_constraints_arriving = statDistribution("NORM", sched_arrive)
        self.__schedule_constraints_leaving = statDistribution("NORM", sched_leave)

    @property
    def arriving_time(self):
        """
        Get the mean arriving time of the PHEV
        :return: a float representing a time instant in seconds
        """
        return self.__schedule_constraints_arriving.equivalent_quantity

    @property
    def leaving_time(self):
        """
        Get the mean leaving time of the PHEV
        :return: a float representing a time instant in seconds
        """
        return self.__schedule_constraints_leaving.equivalent_quantity

    @property
    def discharge_allowed(self):
        """
        Is the discharge of the PHEV battery allowed?
        :return: a Boolean value
        """
        return self.__discharge_allowed

    @discharge_allowed.setter
    def discharge_allowed(self, d):
        """
        Set whether this PHEV battery can be used in discharge mode
        :param d: a Boolean value
        """
        self.__discharge_allowed = d

