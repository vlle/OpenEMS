__author__ = 'Olivier Van Cutsem'

from building_data_management.signal_and_constraint_management.signal_data import PiecewiseConstantSignal

class Constraint(object):
    """
    This class defines a MIN/MAX generic constraint over a quantity.
    """
    def __init__(self, min, max):
        """
        Class constructor
        :param min: the min boundary of the constraint. May be of any type (float, time-series signal, etc)
        :param max: the max boundary of the constraint. May be of any type (float, time-series signal, etc)
        """

        self.__min = min
        self.__max = max

    @property
    def min(self):
        return self.__min

    @min.setter
    def min(self, m):
        self.__min = m

    @property
    def max(self):
        return self.__max

    @max.setter
    def max(self, m):
        self.__max = m


class TimeVaryingConstraint(Constraint):
    """
    This class defines a MIN/MAX time-dependent constraint over a quantity.
    """
    def __init__(self, t, min, max):
        """
        Class constructor
        :param t: an ordered list containing the time instant at which the constraint changes
        :param min: an ordered list containing the min values of the constraint over time
        :param max: an ordered list containing the max values of the constraint over time
        Example:
        t= [0, 12, 24] | min = [22, 18] | max = [26, 30]
        Define a time-varying constraint where:
         - [min, max] = [22, 26] when t is in [0, 12]
         - [min, max] = [18, 30] when t is in [12, 24]
        """
        min_signal = PiecewiseConstantSignal(t, min)
        max_signal = PiecewiseConstantSignal(t, max)
        super(TimeVaryingConstraint, self).__init__(min=min_signal, max=max_signal)

    def get_min(self, t):
        return self.min.get(t)

    def get_max(self, t):
        return self.max.get(t)