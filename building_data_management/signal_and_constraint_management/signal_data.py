__author__ = 'Olivier Van Cutsem'

import math
import numpy as np


class PiecewiseConstantSignal:
    """
    This class represents a signal y that stays constant between consecutive points of x.
    Example:
        x = [0, 12, 24]
        y = [100, 120]
        then the signal is:
        - 100 when the entry v is in [0, 12]
        - 120 when the entry v is in [12, 24]
    """
    def __init__(self, x, y, end_x=None):
        """
        Constructor
        :param x: an ordered list which is the time vector representing the time instants when the signal value changes
        :param y: an ordered list which stores the signal values corresponding to the time instants in x
        :param [end_x]: a numerical value that specifies the time instant when the signal stop.
                        If not specified, the last time instant of the signal is taken as the last value in x.
        """
        self.__x = list(x)
        self.__y = list(y)

        if end_x is not None:  # the format (x,y) isn't appropriate for representing completely the signal: duplicate the last element
            self.__x.append(end_x)
            self.__y.append(self.__y[-1])

    def append(self, dx, v):
        """
        Add a new pair to the signal
        :param dx: a numerical number representing the period when the signal takes the value "v"
        :param v: a numerical number representing the signal value
        """
        if len(self.__x) == 0:
            self.__x[0] = 0
            self.__y[0] = 0

        self.__y[-1] = v  # erase last copy to copy the new value

        # Add new pair
        self.__x.append(self.__x[-1]+dx)  # last x + dx
        self.__y.append(v)

    def get_min_dx(self):
        return min([x1-x0 for x1, x0 in zip(self.__x[1:], self.__x[0:-1])])

    def get(self, v):
        """
        Return the value in y associated to the time instant v
        :param v: a numerical value
        :return: a numerical value corresponding to the signal value at time v
        Remark: if v is not initially in x, it is modified to be within the bound of x
        """

        if not (v < self.__x[-1] and v > self.__x[0]):
            period = (self.__x[-1] - self.__x[0])
            v_in_period = math.fabs(v - self.__x[0]) % period

            if v > self.__x[0]:
                v = self.__x[0] + v_in_period
            else:
                v = period - v_in_period

        x_array = np.array(self.__x)
        x_lower = x_array[x_array <= v]
        x_close = x_lower.max()
        idx = self.__x.index(x_close)

        #return self.__y[int(numpy.searchsorted(self.__x, v, side='right'))]
        return self.__y[idx]

    def get_interval_val(self, x_0, x_1, dx, sampling=False):
        """
        Return a time-series signal representing the initial signal within the bounds [x_0, x_1[ at the sampling period dx
        :param x_0: a numerical value that represents the starting time of the sub-period of interest
        :param x_1: a numerical value that represents the end time of the sub-period of interest
        :param dx: a numerical value that represents the time interval between two values in the sub-period of interest
        :return: a list of numerical values representing the signal within [x_0, x_1[, spaced by dx
        Remark: if x_0/x_1 are not initially in x, it is modified to be within the bounds of x
        """

        if self.get_min_dx() > dx or sampling is True:  # OVER-SAMPLING: take closest values from the original vector
            return self.over_sampling(x_0, x_1, dx)
        else:  # DOWN-SAMPLING: average the events in a window
            return self.signal_interpolation(x_0, x_1, dx)

    def over_sampling(self, x_0, x_1, dx):

        # The signal limits
        x_end = self.__x[-1]
        x_start = self.__x[0]

        nb_pts = int(math.ceil(float((x_1 - x_0)) / float(dx)))
        ret = nb_pts * [0]

        x = x_0 % (x_end - x_start)
        for i in range(0, nb_pts):
            ret[i] = self.get(x)

            x += dx
            if x >= x_end:
                x = x_start + (x % (x_end - x_start))  # x_start + the residu

        return ret

    def signal_interpolation(self, x_0, x_1, dx):
        """
        TODO: describe
        :param x_0:
        :param x_1:
        :param dx:
        :return:
        """

        dx = float(dx)

        # The signal limits
        nb_pts = int(math.ceil(float((x_1 - x_0)) / dx))

        # Find the first event in the list
        x_0_sig = self.sampling_time_in_signal(x_0, 0)
        x_array = np.array(self.__x)
        x_lower = x_array[x_array <= x_0_sig]
        x_close = x_lower.max()
        idx_cur_event = self.__x.index(x_close)
        diff_x0 = x_0_sig-x_close

        # Output index and vector
        i_slot = 0
        y_out = nb_pts * [0.0]

        # LOOP OVER THE EVENTS
        x_start_event = x_0 - diff_x0
        x_end_event = x_start_event + (self.__x[idx_cur_event + 1] - self.__x[idx_cur_event])  # No risk of overflow because idx_cur_event != LAST INDEX

        while i_slot < nb_pts:

            v = self.__y[idx_cur_event]

            if x_end_event < x_0 + (i_slot+1) * dx:  # This event is TOTALLY included in the slot i
                y_out[i_slot] += (x_end_event-x_start_event)/dx * v

            else:  # This event spans several slots

                y_out[i_slot] += (x_0 + (i_slot+1) * dx - x_start_event) / dx * v  # finish this slot
                i_slot += 1  # Pass to the next slot

                # Browse all the next slots until this event finishes
                while x_end_event > x_0 + (i_slot+1) * dx and i_slot < nb_pts:
                    y_out[i_slot] = v
                    i_slot += 1

                if i_slot < nb_pts: # Maybe we reached the end of the needed vector
                    y_out[i_slot] += (x_end_event - x_0 - i_slot * dx) / dx * v  # finish this event

            idx_cur_event += 1  # Pass to the next event in the list, except the last one

            if idx_cur_event == len(self.__x) - 1:
                idx_cur_event = 0

            # Update the absolute event time
            x_start_event = x_end_event
            x_end_event += (self.__x[idx_cur_event + 1] - self.__x[idx_cur_event])

        return y_out

    def sampling_time_in_signal(self, x_0, ddx):
        """
        From value in the required output VECTOR to DATA STRUCTURE
        :param x_0:
        :param ddx:
        :return:
        """

        return (x_0 + ddx) % (self.__x[-1] - self.__x[0])

class SampledSignal:

    def __init__(self, x, y):

        self.__dt = x[1] - x[0]
        self.__t_0 = x[0]
        self.__y = y

    def resample(self, new_dt, t_init=None, t_end=None, sampling=False):

        if new_dt == 0 or self.__dt == 0:
            return None

        x_origin_max = self.__dt * (len(self.__y)-1)
        if t_init is None:
            t_init = self.__t_0
        if t_end is None:
            t_end = self.__t_0 + x_origin_max

        if not (t_init >= self.__t_0 and t_init <= x_origin_max):
            period = (x_origin_max - self.__t_0)
            v_in_period = math.fabs(t_init - self.__t_0) % period
            t_init_aux = t_init
            if t_init > self.__t_0:
                t_init = self.__t_0 + v_in_period
                t_end -= t_init_aux - t_init
            else:
                t_init = period - v_in_period
                t_end += t_init_aux - t_init

        nb_pts = int(t_end-t_init)//new_dt + 1
        new_y = nb_pts * [0]

        idx = int((t_init-self.__t_0)//self.__dt)

        x_origin = self.__dt * idx  # initialize the x origin
        x_new = t_init  # initialize the x origin
        for i in range(nb_pts):
            while x_new >= x_origin+self.__dt:  # while: in case self.__dt < new_dt
                idx += 1
                x_origin += self.__dt
                if idx >= len(self.__y):
                    idx = 0

            new_y[i] = self.__y[idx]
            x_new += new_dt

        return new_y


# TEST
#
# x_test = [0, 180.0, 3600.0]
# y_test = [0.19, 0, 0]
# test_obj = PiecewiseConstantSignal(x_test, y_test)
#
# out = test_obj.signal_interpolation(900, 8100.0, 900.0)
# print(out)
#
# import matplotlib.pyplot as plt
# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.plot([out[0]] + out, drawstyle='steps')
#
# plt.show()