__author__ = 'Olivier Van Cutsem'

from building_data_management.category_management.category_config import *
from ems_config import *
import copy

from multiprocessing import Queue
import Queue

EMS_GUI_AGGREGATE_PLOT_LIMIT = 4

class EMS_ANALYZER():
    """
    User interface for EMS class
    """
    MSG_TYPE_KEY = 'MSG_TYPE'
    MSG_TYPE_INIT = 'init_gui'
    MSG_TYPE_ADD = 'new_data'
    MSG_TYPE_PLOT = 'plot_data'

    MSG_CONTENT_DATA = 'data'
    MSG_CONTENT_TIME = 'timestamp'

    MSG_EMS_STATE_STRUCT_ENERGY = 'energy'
    MSG_EMS_STATE_STRUCT_COMFORT = 'comfort'
    MSG_EMS_STATE_STRUCT_CONSTRAINTS = 'constraints'
    MSG_EMS_STATE_STRUCT_REF_VAL = 'reference_value'
    MSG_EMS_STATE_STRUCT_EMS_LEVEL = 'ems_level'

    def __init__(self):

        self.data_storage = EMS_ANALYZER.get_data_format()  # this variable storage all the incoming data

    def analyse_data(self, t, data):
        """
        Store incoming data
        :param data:
        :return:
        """

        # Analyse the received ems state data

        # Energy data

        if EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY in data.keys():
            energy_data = data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY]
            obj_energy = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY]
            for type_entity in energy_data.keys():

                if type_entity != EMS_CATEGORY_ENTITY_STORAGE:  # just power
                    for name_entity in energy_data[type_entity].keys():
                        new_data = (t, energy_data[type_entity][name_entity])  # pair (time, value)
                        if name_entity in obj_energy[type_entity].keys():  # already registered entity
                            obj_energy[type_entity][name_entity].append(new_data)
                        else:  # otherwise create a new list
                            obj_energy[type_entity][name_entity] = [new_data]

                else:
                    for name_entity in energy_data[type_entity][EMS_ENERGY_power].keys():
                        new_data_p = (t, energy_data[type_entity][EMS_ENERGY_power][name_entity])  # pair (time, value)
                        if name_entity in obj_energy[type_entity][EMS_ENERGY_power].keys():  # already registered entity
                            obj_energy[type_entity][EMS_ENERGY_power][name_entity].append(new_data_p)
                        else:  # otherwise create a new list
                            obj_energy[type_entity][EMS_ENERGY_power][name_entity] = [new_data_p]

                    for name_entity in energy_data[type_entity][EMS_ENERGY_soc].keys():
                        new_data_soc = (t, energy_data[type_entity][EMS_ENERGY_soc][name_entity])  # pair (time, value)
                        if name_entity in obj_energy[type_entity][EMS_ENERGY_soc].keys():  # already registered entity
                            obj_energy[type_entity][EMS_ENERGY_soc][name_entity].append(new_data_soc)
                        else:  # otherwise create a new list
                            obj_energy[type_entity][EMS_ENERGY_soc][name_entity] = [new_data_soc]

        # Comfort
        if EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT in data.keys():
            comfort_data = data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT]
            obj_comfort = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT]
            for room_name in comfort_data.keys():
                if room_name not in obj_comfort.keys():  # first time seeing this room?
                    obj_comfort[room_name] = {}

                for comfort_type in comfort_data[room_name].keys():
                    new_data = (t, comfort_data[room_name][comfort_type])  # pair (time, value)
                    if comfort_type in obj_comfort[room_name].keys():  # already registered entity
                        obj_comfort[room_name][comfort_type].append(new_data)
                    else:  # otherwise create a new list
                        obj_comfort[room_name][comfort_type] = [new_data]

        # Constraint data
        if EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS in data.keys():
            constr_data = data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS]
            obj_constr = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS]
            for room_name in constr_data.keys():
                if room_name not in obj_constr.keys():  # first time seeing this room?
                    obj_constr[room_name] = {}

                for comfort_type in constr_data[room_name].keys():
                    new_data_min = (t, constr_data[room_name][comfort_type]['min'])  # pair (time, value)
                    new_data_max = (t, constr_data[room_name][comfort_type]['max'])  # pair (time, value)
                    if comfort_type in obj_constr[room_name].keys():  # already registered entity
                        obj_constr[room_name][comfort_type]['min'].append(new_data_min)
                        obj_constr[room_name][comfort_type]['max'].append(new_data_max)
                    else:  # otherwise create a new list
                        obj_constr[room_name][comfort_type] = {}
                        obj_constr[room_name][comfort_type]['min'] = [new_data_min]
                        obj_constr[room_name][comfort_type]['max'] = [new_data_max]

        # Ref value
        if EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL in data.keys():
            ref_data = data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL]
            obj_ref = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL]

            for room_name in ref_data.keys():
                if room_name not in obj_ref.keys():  # first time seeing this room?
                    obj_ref[room_name] = {}

                for comfort_type in ref_data[room_name].keys():
                    new_data_ref = (t, ref_data[room_name][comfort_type])  # pair (time, value)
                    if comfort_type in obj_ref[room_name].keys():  # already registered entity
                        obj_ref[room_name][comfort_type].append(new_data_ref)
                    else:  # otherwise create a new list
                        obj_ref[room_name][comfort_type] = [new_data_ref]

        # Environment data / EMS-LEVEL DATA
        if EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL in data.keys():
            env_data = data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL]
            obj_env = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL]

            for env_data_type in env_data.keys():
                new_data_env = (t, env_data[env_data_type])  # pair (time, value)
                if env_data_type in obj_env.keys():  # already registered entity
                    obj_env[env_data_type].append(new_data_env)
                else:  # otherwise create a new list
                    obj_env[env_data_type] = [new_data_env]


    @staticmethod
    def get_data_format():

        data = dict()

        # loads, storage, generation
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_LOAD] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_power] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_soc] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_GENERATION] = {}

        # rooms
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS] = {}
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL] = {}

        # Environement
        data[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL] = {}

        return data

    def get_aggregated_power_timeseries(self, type_data):
        """

        :param type_data:
        :return:
        """

        if type_data not in [EMS_CATEGORY_ENTITY_LOAD, EMS_CATEGORY_ENTITY_GENERATION, EMS_CATEGORY_ENTITY_STORAGE]:  # basic data
            return [], []

        if type_data != EMS_CATEGORY_ENTITY_STORAGE:
            raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][type_data]
        else:
            raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][type_data][EMS_ENERGY_power]

        acc = []
        for _name in raw_data.keys():
            if acc == []:
                acc = copy.deepcopy(raw_data[_name])
            else:
                for i in range(0, len(raw_data[_name])):
                    acc[i] = (acc[i][0], acc[i][1] + raw_data[_name][i][1])

        return list([t for (t, v) in acc]), list([v for (t, v) in acc])

    def get_grid_power_aggregated_timeseries(self):

        # todo: don't assume a constant timesampling ..

        list_entities_type = [EMS_CATEGORY_ENTITY_LOAD, EMS_CATEGORY_ENTITY_STORAGE, EMS_CATEGORY_ENTITY_GENERATION]

        # Total purchased
        # Load + Batt + Gen
        acc_grid_purch = []
        acc_t = []
        for e in list_entities_type:

            acc_aux_t, acc_data_aux = self.get_aggregated_power_timeseries(e)

            if acc_aux_t == []:  # this type of quantity is not present
                continue
            else:
                acc_t = acc_aux_t

            if acc_grid_purch == []:
                acc_grid_purch = acc_data_aux
            else:
                acc_grid_purch = ([v1 + v2 for (v1, v2) in zip(acc_data_aux, acc_grid_purch)])

        # Total sold
        # Remove the part sold to the grid
        data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL][EMS_LEVEL_DATA_SOLD_ELEC]
        acc_grid_sold = []
        if data != []:
            acc_grid_sold = list([v for (t, v) in data])
            acc_grid_purch = list([v1 - v2 for (v1, v2) in zip(acc_grid_purch, acc_grid_sold)])

            # Add the end, if acc_grid_purch has negative components, transfer them to the sold elec
            acc_grid_sold = list([v1 + min(v2, 0) for (v1, v2) in zip(acc_grid_sold, acc_grid_purch)])
            acc_grid_purch = list([max(v, 0) for v in acc_grid_purch])

        return (acc_t, acc_grid_purch), (acc_t, acc_grid_sold)

    def get_indivual_power_timeseries(self, type_data):

        if type_data != EMS_CATEGORY_ENTITY_STORAGE:
            raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][type_data]
        else:
            raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][type_data][EMS_ENERGY_power]

        list_data = {}
        for _name, _v in raw_data.items():
            list_data[_name] = (list([t for (t, v) in _v]), list([v for (t, v) in _v]))

        return list_data

    def get_price_timeseries(self):
        data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL][EMS_ENV_DATA_ELEC_PRICE]
        return list([t for (t, v) in data]), list([v for (t, v) in data])

    ### Comfort manipulation ###

    def get_rooms_comfort_values_timeseries(self, type_comf):

        room_list_data = dict()
        # raw values
        raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT]

        for _r, _v in raw_data.items():

            if type_comf in _v.keys():
                room_list_data[_r] = (list([t for (t, v) in _v[type_comf]]), list([v for (t, v) in _v[type_comf]]))

        return room_list_data

    def get_rooms_comfort_constraint_timeseries(self, room_id, type_comf):

        room_list_data = dict()
        ret = {'min': [], 'max': []}

        # raw values
        raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS]
        if room_id not in raw_data or type_comf not in raw_data[room_id]:
            return ret

        raw_data = raw_data[room_id][type_comf]

        ret['min'] = list([t for (t, v) in raw_data['min']]), list([v for (t, v) in raw_data['min']])
        ret['max'] = list([t for (t, v) in raw_data['max']]), list([v for (t, v) in raw_data['max']])

        return ret

    def get_rooms_comfort_reference_timeseries(self, room_id, type_comf):

        room_list_data = dict()
        # raw values
        raw_data = self.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL]
        if room_id not in raw_data or type_comf not in raw_data[room_id]:
            return [], []

        raw_data = raw_data[room_id][type_comf]

        return list([t for (t, v) in raw_data]), list([v for (t, v) in raw_data])
