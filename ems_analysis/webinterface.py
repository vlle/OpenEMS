__author__ = 'Olivier Van Cutsem'

from building_data_management.category_management.category_config import *
from building_data_management.room_data import BuildingRoomData
import time

EMS_WEBINTERFACE_DWEET_NAME = 'ems_state'

def dweet_create_interface():
    #rep = dweepy.dweet_for(EMS_WEBINTERFACE_DWEET_NAME)
    pass

def dweet_add_value(dict_state):
    rep = dweepy.dweet_for(EMS_WEBINTERFACE_DWEET_NAME, dict_state)

def dweet_interface_update(ems_state):
    """
    ems_state is a BuildingDataManagementSystem object
    :param ems_state:
    :return:
    """

    state = dict()

    # Loads, generation and storage
    _loads = ems_state.get_entity_list(EMS_CATEGORY_ENTITY_LOAD)
    _stor = ems_state.get_entity_list(EMS_CATEGORY_ENTITY_STORAGE)
    _gen = ems_state.get_entity_list(EMS_CATEGORY_ENTITY_GENERATION)

    for (l_id, l_obj) in _loads:
        state[l_obj.name+".power"] = l_obj.current_power

    for (s_id, s_obj) in _stor:
        state[s_obj.name+".power"] = s_obj.current_power
        state[s_obj.name+".energy"] = s_obj.current_soc

    for (g_id, g_obj) in _gen:
        state[g_obj.name+".power"] = g_obj.current_power


    # Room comfort
    for _r in ems_state.room_list:
        room_name = _r.name
        list_of_comfort = BuildingRoomData.TYPE_COMFORT_LIST
        for type_comfort in list_of_comfort:
            v = _r.get_comfort_value(type_comfort)
            if v is not None:
                state[room_name+"."+type_comfort] = v

    dweet_add_value(state)
    time.sleep(1)

