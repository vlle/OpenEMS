__author__ = 'Olivier Van Cutsem'

# GENERAL CONFIGURATION
EMS_TIME_STEP = 10*60  # in seconds
SIMULATION_DAY = 21  # SHOULD BE THE SAME THAN THE V_ENGINE
EMS_UNIT = 7  # Single-room Unit
EMS_VERBOSE = False  # make the EMS speak for debug
EMS_STOP_UPON_SIMULATION_END = True  # Close EMS when the simulation is over

SIMULATION_GUI_CONNECTED = True  # True if the GUI launches the OpenEMS

# Energy planning config
EMS_ENERGY_PLANNING_PERIOD = 24*60*60

# GUI preferences
EMS_GUI_PLOT_COMFORT = ['individual', 'per_quantity', 'per_room']
EMS_GUI_PLOT_ENERGY = ['individual', 'per_type', 'aggregated']
EMS_GUI_SELECT_TYPE = {'comfort': 1, 'energy': [2]}

# EMS events nature
EMS_MODE_VE_SIMULATION = "VE_SIMULATION"  # VE_SIMULATION: simulation through the vEngine
EMS_MODE_RT_EVENT_DRIVEN = "RT_EVENT_DRIVEN"  # VE_SIMULATION: simulation through the vEngine
EMS_MODE_RT_SAMPLED = "RT_SAMPLED"  # VE_SIMULATION: simulation through the vEngine
EMS_MODE = EMS_MODE_VE_SIMULATION

# EMS events protocol - RT-event interface

RT_EVENT_PROTOCOL_ZMQ = 'RT-EVENT-ZMQ'
RT_EVENT_PROTOCOL_WEBSOCKET = 'RT-EVENT-WEBSOCKET'
RT_EVENT_PROTOCOL = RT_EVENT_PROTOCOL_ZMQ

# OPEN BMS CONNECTION: API
OPENBMS_IP = '128.178.19.163'  #128.178.19.240
OPENBMS_PORT = '80'

# OPEN BMS CONNECTION: RT server
RT_SERVER_IP = OPENBMS_IP
RT_SERVER_PORT = '8000'

WEBSOCKET_ZMQ_MSG_TYPE = "ZMQ_MSG" #for messages arriving through the zmq loop
WEBSOCKET_WEB_MSG_TYPE = "WEB_MSG" #for messages arriving through the websockets loop

# ZMQ_PORT
ZMQ_SIMU_IP = '127.0.0.1'
ZMQ_BMS_PORT_PUB = '51411'
ZMQ_BMS_PORT_SUB = '51500'
SOCKET_SETTLE_TIME = 2

# RT interface - EMS communication msg type
RT_EVENT_MSG_TYPE_KEY = 'RT_MSG_TYPE'
RT_EVENT_MSG_TYPE_MEASURE_STATUS = 'MEASURE_STATUS_MSG'
RT_EVENT_MSG_TYPE_SIMULATION_SYNQ = 'SIMU_MSG'

  # For measures
RT_EVENT_SENDER_ID = 'event_sender_id'
RT_EVENT_SENDER_TYPE = 'event_sender_nature'
RT_EVENT_SENDER_TYPE_SENSOR = 'event_from_sensor'
RT_EVENT_SENDER_TYPE_ACTUATOR = 'event_from_actuator'
RT_EVENT_QUANTITY_TYPE = 'event_quantity_type'
RT_EVENT_VALUE = 'event_value'

  # For simulation purpose
RT_EVENT_COORDINATION_MSG_TYPE = 'simulation_coordination'

# From the EMS to the RT-INTERFACE
EMS_DOWNSTREAM_MSG_TYPE = 'MSG_TYPE'
EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND = 'entity_command'
EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND_TYPE = 'entity_command_type'
EMS_DOWNSTREAM_SIMU_READY = 'simulation_ready'
EMS_DOWNSTREAM_MSG_ENTITY_TYPE = 'entity_type'
EMS_DOWNSTREAM_MSG_ENTITY_ID = 'entity_id'
EMS_DOWNSTREAM_MSG_PAYLOAD = 'payload'
EMS_DOWNSTREAM_MSG_NEXT_STEP = 'next_controller_step'

# EMS in SIMULATION mode
SIMULATION_TIME_STEP = 5*60  # MUST BE IN ACCORDANCE WITH THE SIMULATOR !! #TODO: the simulator sends current time..
SIMULATION_BMS_MID_ID = 13
SIMULATION_VMID_ADDRESS = 0
SIMULATION_DATA_FILENAME = 'C:/Users/vancutse/PycharmProjects/elab-bms-virtual/virtualization/vE_models/data_files/'


RT_EVENT_SIMULATION_PARAM_KEY = 'SIMU_PARAM'
RT_EVENT_SIMULATION_PARAM_STOP = 'SIMU_PARAM_STOP'

# EMS ENERGY and COMFORT QUANTITIES
EMS_ENERGY_soc = 'SOC'
EMS_ENERGY_power = 'POWER'
EMS_ENERGY_nb_cycle = 'CYCLES'
EMS_ENERGY_mode = 'MODE'
EMS_COMFORT_temperature = 'TEMP'
EMS_COMFORT_luminosity = 'LUM'
EMS_COMFORT_presence = 'PRE'
EMS_COMFORT_humidity = 'HUM'
EMS_COMFORT_irradiance = 'IRR'
EMS_LIST_ENERGY_RELATED_QUANTITIES = [EMS_ENERGY_soc, EMS_ENERGY_power]
EMS_LIST_COMFORT_QUANTITIES = [EMS_COMFORT_temperature, EMS_COMFORT_luminosity,EMS_COMFORT_presence, EMS_COMFORT_humidity]

# EMS ENVIRONMENTAL DATA / EMS-LEVEL DATA

EMS_ENV_DATA_TEMP = 'outside_temperature'
EMS_ENV_DATA_IRR = 'outside_irradiance'
EMS_ENV_DATA_LUM = 'outside_luminosity'

EMS_ENV_DATA_ELEC_PRICE = 'electricity_price'
EMS_ENV_DATA_ELEC_SELLING_PRICE = 'electricity_selling_price'
EMS_LEVEL_DATA_SOLD_ELEC = 'electricity_sold'

# EMS/BMS ROOM: TYPE AND NATURE OF THE SPACE
EMS_ROOM_NATURE_HUMAN_SPACE = 'Human space'
EMS_ROOM_NATURE_WATER_ENERGY_STORAGE = 'Energy storage'
EMS_ROOM_NATURE_ENVIRONMENT = 'Energy Environment'
EMS_ROOM_NATURE_FROM_DB = {'STANDARD': 'Human space',
                           'ENERGY': 'Energy storage',
                           'ENV': 'Environment'}

EMS_ROOM_TYPE_WATER_CIRCUIT = 'Water Circuit'
EMS_ROOM_TYPE_WATER_TANK = 'Water Tank'
EMS_ROOM_TYPE_OUTSIDE = 'Outside'
EMS_ROOM_TYPE_REGULAR_ROOM = 'Regular'
EMS_ROOM_TYPE_FROM_DB = {6: EMS_ROOM_TYPE_WATER_CIRCUIT,
                         5: EMS_ROOM_TYPE_WATER_TANK,
                         3: EMS_ROOM_TYPE_OUTSIDE}
EMS_OUTSIDE_ROOM_NAME = 'Outside'
