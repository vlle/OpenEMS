__author__ = 'Olivier Van Cutsem'

from threading import Thread

from ems_config import *
from building_data_management.bdms_main import BuildingDataManagementSystem
from building_data_management.signal_and_constraint_management.signal_data import PiecewiseConstantSignal, SampledSignal
from building_data_management.category_management.category_config import *
from rtevents_interface.rtevents_interf import *

if SIMULATION_GUI_CONNECTED:
    from frontend.interface import format_log_msg, GUI_LOG_SENDER_NAME_EMS, GUI_LOG_TYPE_INFO, GUI_LOG_TYPE_DATA, GUI_LOG_TYPE_TIMING

from ems_analysis.main_analysis import *

import json
from abc import abstractmethod
import time
from multiprocessing import Queue
from building_data_management.ambient_data import EnvironmentalDataManagement
from building_data_management.room_data import BuildingRoomData

from opti_model.opti_model import LinearOptiBuildingModel
from opti_model.main_loadscheduler import LoadScheduler


class EnergyManagementSystem(Thread):

    """

    Mother class for all the EMS strategies

    - It instantiates a Building Data Management System (building info and R-T data)
    - It instantiates a Environmental Data Management (electricity price, etc)
    - It connects to the RT-events interfaces for polling data and pushing commands

    ABSTRACT method to implement: update()

    """

    def __init__(self, queues, unit=None, ems_time_step=None, simu_starting_day=None, data_path=None):
        """
        Connect messaging queues | Initialize empty structures
        :param queues: a tuple containing:
                in_q: queue for collecting messages coming from the RT-event interface
                out_q: queue for sending messages to the RT-event interface
                gui_q: queue for sending messages to the GUI interface
        :param unit: an integer standing for the BMS unit to be loaded
        :param ems_time_step: a numerical value that is the time step between two control actions of the EMS
        :param simu_starting_day: an integer value in [0, 365] that stands for the number in the day of the starting day of the simulation
        :param data_path: a string holding the absolute path to the data necessary for the EMS control
        """
        in_q, out_q, gui_q = queues
        Thread.__init__(self)
        self.in_stream = in_q
        self.out_stream = out_q
        self.gui_data = gui_q

        # Configuration & simulation time
        if unit is not None:
            self.unit = unit
        else:
            self.unit = EMS_UNIT

        if ems_time_step is not None:
            self.ems_dt = ems_time_step
        else:
            self.ems_dt = EMS_TIME_STEP

        if simu_starting_day is not None:
            self.starting_day = simu_starting_day
        else:
            self.starting_day = SIMULATION_DAY

        ## General data
        self.general_info = {}

        ## Clustering and Data Structure system
        self.building_data = BuildingDataManagementSystem()  # The empty BD-MS

        ## Environmental data storage
        self.environment_data = EnvironmentalDataManagement(data_path)  # The hardcoded environmental data

        ## Environmental data storage
        self.linopti_model = LinearOptiBuildingModel()  # The load scheduler logic
        self.current_energy_planning = None

        ## EMS running mode and time configuratin
        # In RT control mode, keep track of the current time ; In simulation mode, keep track of the simulation time
        if EMS_MODE != EMS_MODE_VE_SIMULATION:
            self.current_time = time.time()
        else:
            self.current_time = self.starting_day * 24 * 60 * 60

        if EMS_MODE == EMS_MODE_VE_SIMULATION:
            self.simu_param = None
            self.simu_status_run = True  # run the simulation

        self.last_ems_step = self.current_time
        self.simu_status_run = True

        # State tracking with commands
        self.current_state = EMS_ANALYZER.get_data_format()
        self.current_command_sent = list()

        # The part of generation that is sold to the grid
        self.current_selling_gen = 0

        # Energy planning variables
        self.last_planning = -float('inf')

        # Statistics about the EMS
        self.stat = {'update_count': 0, 'list_update_dur': []}

    def run(self):
        """
        EMS main loop:
        @init: BMS request for CMS data structure filling
        @main_loop: check the incoming message to update BDMS and then run the EMS control step
        """

        # INITIALIZATION of the models
        ret_code = self.init_ems_data()
        if ret_code != 0:
            payload = 'An error occurred during CMS initialization, shutting down EMS thread ...'
            if SIMULATION_GUI_CONNECTED:
                self.push_to_gui(type_msg=GUI_LOG_TYPE_INFO, msg=payload)
            else:
                print(payload)

            return

        payload = 'Entering into the main loop, waiting for the Simulator ...'
        if SIMULATION_GUI_CONNECTED:
            self.push_to_gui(type_msg=GUI_LOG_TYPE_INFO, msg=payload)
        else:
            print(payload)

        while True:

            # This boolean decides whether to run the next EMS control step or not within this loop iteration
            run_ems_step = False
            run_planning_step = False

            if EMS_MODE == EMS_MODE_RT_SAMPLED:  # The EMS is in Sampled mode
                if self.current_time - self.last_ems_step >= self.ems_dt:
                    run_ems_step = True

            if self.current_time - self.last_planning >= EMS_ENERGY_PLANNING_PERIOD:
                run_planning_step = True

            # CHECK FOR NEW MESSAGE
            try:
                msg = self.in_stream.get(EMS_MODE == EMS_MODE_VE_SIMULATION)  # block in simulation mode
            except Queue.Empty:
                msg = None

            if msg is not None:

                if RT_EVENT_MSG_TYPE_KEY in msg:
                    if msg[RT_EVENT_MSG_TYPE_KEY] == RT_EVENT_MSG_TYPE_MEASURE_STATUS:  # new measure/status for the CMS
                        if EMS_MODE == EMS_MODE_RT_EVENT_DRIVEN:
                            run_ems_step = True

                        if EMS_VERBOSE:
                            print("[{1}s] EMS received: {0}".format(msg, self.current_time))
                        # Process this event in order to update the appropriate structures
                        self.decode_rtevent(msg)

                    elif msg[RT_EVENT_MSG_TYPE_KEY] == RT_EVENT_MSG_TYPE_SIMULATION_SYNQ:  # Synchronisation msg for simu

                        # Analyse the incoming simulation-related message
                        self.decode_simulation_event(msg)

                        if self.simu_status_run is True:
                            run_ems_step = True

                        # Update the interface data
                        self.send_gui_state()

                        if self.simu_status_run is False and EMS_STOP_UPON_SIMULATION_END is True:
                            return True

            # RUN THE ENERGY PLANNING STEP
            if run_planning_step:
                self.update_planning()
                self.last_planning = self.current_time

                payload = 'Energy scheduler results: TODO'
                if SIMULATION_GUI_CONNECTED:
                    self.push_to_gui(type_msg=GUI_LOG_TYPE_INFO, msg=payload)
                else:
                    print(payload)

            # RUN THE EMS CONTROL STEP
            if run_ems_step:

                # The CORE of the EMS class: call the update class for updating model and generating commands
                self.stat['update_count'] += 1
                timer1 = time.time()
                ems_commands = self.update()
                self.stat['list_update_dur'].append(time.time()-timer1)

                # Triggered the scheduled loads:
                list_of_loads = self.linopti_model.load_scheduler.get_loads_to_trigger(self.current_time)
                for load_id in list_of_loads:
                    ems_commands.append(self.trigger_load(load_id))

                if ems_commands is not None:
                    self.send_commands(ems_commands)

                # in case of simulation, send the READY signal
                if EMS_MODE == EMS_MODE_VE_SIMULATION:
                    self.last_ems_step = self.current_time
                    self.current_time += self.ems_dt
                    ready_signal = {EMS_DOWNSTREAM_MSG_TYPE: EMS_DOWNSTREAM_SIMU_READY,
                                    EMS_DOWNSTREAM_MSG_NEXT_STEP: self.current_time}
                    self.out_stream.put(ready_signal)

            if not(EMS_MODE == EMS_MODE_VE_SIMULATION):
                self.last_ems_step = self.current_time
                self.current_time = time.time()


                                                ##########################
                                            ######### INITIALIZATION #########
                                                ##########################

    def send_commands(self, cmd):
        """
        Send a set of commands downstream and store the target entities
        :param cmd: a formatted list, containing EMS commands
        :return:
        """

        for command in cmd:

            # Send the commands to the system
            self.out_stream.put(command)

            # Remember the targets of the commands
            ve_type = command[EMS_DOWNSTREAM_MSG_ENTITY_TYPE]
            ve_id = command[EMS_DOWNSTREAM_MSG_ENTITY_ID]
            self.current_command_sent.append((ve_type, ve_id))  # The state of those entities is unknown for the moment

    def init_ems_data(self):
        """
        Fill the empty BDMS structure with data coming from the BMS
        :return: 0 if everything went right ; non-zero value otherwise
        """

        # Get the general building information
        self.initFromBMS()

        # Initialize the building data structure through the BMS API
        self.building_data.initFromBMS(self.unit)

        # Initialize the energy planning data
        self.linopti_model.initFromEMS(self.building_data)

        payload = 'Initialized with data of Unit {0} from the OpenBMS'.format(self.unit)
        if SIMULATION_GUI_CONNECTED:
            self.push_to_gui(type_msg=GUI_LOG_TYPE_INFO, msg=payload)
        else:
            print(payload)

        ####### METHOD TO BE OVERWRITTEN
        self.initModelParameters()

        return 0

    def initFromBMS(self):
        """
        Get the general info about the building
        :return:
        """

        ret = get_unit_info(self.unit)

        for k, v in ret.items():
            self.general_info[k] = v

    @abstractmethod
    def initModelParameters(self):
        """
        TODO
        :return:
        """
        pass

    @abstractmethod
    def update(self):
        """
        This abstract method runs the main energy management algorithm.
        :return: a set of commands for the flexible (controllable) building entities
        """
        return []

    def get_time_info(self, t=None):
        """
        Get day, hour and minutes
        :param t: a relative amount of seconds since the beginning of the simulation
        :return: a tuple
        """

        if t is None:  # get the current time
            t = self.current_time

        day_of_year = t % (24*3600)
        hour_of_day = t % 3600
        min_of_hour = t % 60

        return (day_of_year, hour_of_day, min_of_hour)


                                            ##############################
                                        ######### MESSAGE PROCESSING #########
                                            ##############################

    def decode_rtevent(self, msg):
        """
        Analyse a message "msg" coming from the RT-event interface concerning the building state and update the BDMS accordingly
        :param msg: a formatted dictionary containing useful data
        :return: /
        """

        # Type of entity: sensor or actuator ?
        type_sender = msg[RT_EVENT_SENDER_TYPE]
        if type_sender == RT_EVENT_SENDER_TYPE_SENSOR:
            sensor_id = msg[RT_EVENT_SENDER_ID]
            sensing_type = msg[RT_EVENT_QUANTITY_TYPE]
            comfort_value = msg[RT_EVENT_VALUE]

            if sensing_type in EMS_LIST_COMFORT_QUANTITIES:
                # Update the corresponding room
                self.building_data.updateComfortRoom(sensor_id=sensor_id,
                                                     type_comfort=sensing_type,
                                                     value=comfort_value)

            elif sensing_type in EMS_LIST_ENERGY_RELATED_QUANTITIES:
                # Update the corresponding energy-related entity
                self.building_data.updateEnergyRelatedEntity(sensor_id=sensor_id,
                                                             type_energy=sensing_type,
                                                             value=comfort_value)

        elif type_sender == RT_EVENT_SENDER_TYPE_ACTUATOR:
            actuator_id = msg[RT_EVENT_SENDER_ID]
            comfort_type = msg[RT_EVENT_QUANTITY_TYPE]
            comfort_value = msg[RT_EVENT_VALUE]

            pass #todo

    def decode_simulation_event(self, msg_simu):
        """
        Analyse a message "msg" coming from the RT-event interface concerning the simulation and react accordingly
        :param msg_simu: a formatted dictionary
        :return: /
        """

        self.simu_param = msg_simu[RT_EVENT_SIMULATION_PARAM_KEY]

        if self.simu_param is not None:
            if RT_EVENT_SIMULATION_PARAM_STOP in self.simu_param:

                payload = 'Sent STOP signal to the simulator'
                if SIMULATION_GUI_CONNECTED:
                    self.push_to_gui(type_msg=GUI_LOG_TYPE_INFO, msg=payload)
                else:
                    print(payload)

                self.reset_simulation_data()

                self.print_statistics()  # show statistics about the EMS

                # Ask the Graph manager to plot the data
                payload = {EMS_ANALYZER.MSG_TYPE_KEY: EMS_ANALYZER.MSG_TYPE_PLOT}
                if SIMULATION_GUI_CONNECTED:
                    self.push_to_gui(type_msg=GUI_LOG_TYPE_DATA, msg=payload)
                else:
                    self.push_to_gui(payload)
        else:
            self.simu_status_run = True

    def reset_simulation_data(self):
        self.current_time = self.starting_day * 24 * 60 * 60
        self.simu_status_run = False

    def print_statistics(self):

        av_time = 0
        if self.stat['update_count'] > 0:
            av_time = sum(self.stat['list_update_dur'])/self.stat['update_count']

        payload = json.dumps({'nb_update': round(self.stat['update_count'], 3), 'av_update': round(av_time, 3)})
        if SIMULATION_GUI_CONNECTED:
            self.push_to_gui(type_msg=GUI_LOG_TYPE_TIMING, msg=payload)
        else:
            print(payload)

                                            ##########################
                                        ######### Builgin planning #########
                                            ##########################

    def update_planning(self):
        """
        Update the building energy planning
        Generic implementation:
         - Economic
         - Peak reduction
        :return: /
        """

        # Load scheduling
        h = EMS_ENERGY_PLANNING_PERIOD
        price = self.get_elec_price((self.current_time, h, self.ems_dt))

        # Solve the building optimization problem over 24h

        self.current_energy_planning = self.linopti_model.get_energy_planning(time_data=(self.current_time, h, self.ems_dt), elec_price=price, param=None)


                                            ##########################
                                        ######### CORE FUNCTIONS #########
                                            ##########################

    def battery_set_point(self, s_id, new_value):
        """
        Format a message to be sent to the controlled system for setting a battery power
        :param s_id: the ID of the battery
        :param new_value: the new power of the battery (in Watt)
        :return: a dictionary containing the formatted command
        """
        storage_command = {EMS_DOWNSTREAM_MSG_TYPE: EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND,
                           EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND_TYPE: EMS_ENERGY_power,
                           EMS_DOWNSTREAM_MSG_ENTITY_TYPE: EMS_CATEGORY_ENTITY_STORAGE,
                           EMS_DOWNSTREAM_MSG_ENTITY_ID: s_id,
                           EMS_DOWNSTREAM_MSG_PAYLOAD: {EMS_ENERGY_power: new_value}}

        return storage_command

    def hvac_set_point(self, l_id, new_value):
        """
        Format a message to be sent to the controlled system for setting a HVAC power
        :param l_id: the ID of the HVAC
        :param new_value: the new power level of the HVAC (in %)
        :return: a dictionary containing the formatted command
        """
        hvac_command = {EMS_DOWNSTREAM_MSG_TYPE: EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND,
                        EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND_TYPE: EMS_ENERGY_power,
                        EMS_DOWNSTREAM_MSG_ENTITY_TYPE: EMS_CATEGORY_ENTITY_LOAD,
                        EMS_DOWNSTREAM_MSG_ENTITY_ID: l_id,
                        EMS_DOWNSTREAM_MSG_PAYLOAD: {EMS_ENERGY_power: new_value}}

        return hvac_command

    def trigger_load(self, l_id, mode='ON'):

        load_command = {EMS_DOWNSTREAM_MSG_TYPE: EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND,
                        EMS_DOWNSTREAM_ENERGY_ENTITY_COMMAND_TYPE: EMS_ENERGY_mode,
                        EMS_DOWNSTREAM_MSG_ENTITY_TYPE: EMS_CATEGORY_ENTITY_LOAD,
                        EMS_DOWNSTREAM_MSG_ENTITY_ID: l_id,
                        EMS_DOWNSTREAM_MSG_PAYLOAD: {EMS_ENERGY_mode: mode}}

        return load_command

    def set_selling_generation(self, p):
        """
        Set the total generated power that will be sold to the grid
        :param p: a numerical value, in Watt
        :return: /
        """

        self.current_selling_gen = p

    def get_environment_data(self, type_data, time_data):
        """
        Generate a vector of data of type "type_data" for the period described by time_data
        :param type_data: type of data to get, one among types beginning with 'EMS_ENV_DATA_'
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :return: a list of values representing the data of interested in the period described by time_data
        """

        (t_cur, t_hor, t_step) = time_data

        # TODO: Make it dynamic ! for the moment the raw data are encoded in a vector
        if type_data == EMS_ENV_DATA_IRR:

            first_data = self.environment_data.environmentIrr['v'][0]
            if type(first_data) is not tuple:
                sig_object = SampledSignal(self.environment_data.environmentIrr['t'],
                                           self.environment_data.environmentIrr['v'])
                return sig_object.resample(t_step, t_cur, t_cur + t_hor)

            else:  # dealing with DIFF and DIRECT irrandiance
                irr_comp_list = zip(*self.environment_data.environmentIrr['v'])

                sig_object_direct = SampledSignal(self.environment_data.environmentIrr['t'],
                                                  irr_comp_list[0])
                dir_list = sig_object_direct.resample(t_step, t_cur, t_cur + t_hor)

                sig_object_diff = SampledSignal(self.environment_data.environmentIrr['t'],
                                                irr_comp_list[1])
                diff_list = sig_object_diff.resample(t_step, t_cur, t_cur + t_hor)
                return zip(dir_list, diff_list)


        elif type_data == EMS_ENV_DATA_TEMP:
            sig_object = SampledSignal(self.environment_data.environmentTemp['t'],
                                       self.environment_data.environmentTemp['v'])
            return sig_object.resample(t_step, t_cur, t_cur + t_hor)
        elif type_data == EMS_ENV_DATA_ELEC_PRICE:
            data_raw = self.environment_data.elecPrice
            sig_object = PiecewiseConstantSignal(data_raw['t'], data_raw['v'])

            return sig_object.get_interval_val(t_cur, t_cur + t_hor, t_step)
        else:
            return None

    def get_elec_price(self, time_data):
        """
        Generate a vector of electricity prices for the period described by time_data
        :param time_data: a tuple (t_0, t_hor, t_step) where:
                - t_0 is the instant corresponding to the first value of the returned vector (in seconds)
                - t_hor is the length of the time period of the returned vector of data
                - t_step is the time interval between two values of the returned vector of data
        :return: a list of values representing the electricity prices in the period described by time_data
        """
        return self.get_environment_data('electricity_price', time_data)

    def get_thermal_load_room(self, th_l_id):
        """
        Get the room ID that contains the thermal load 'th_l_id'
        :param th_l_id: the ID of the thermal load to locate
        :return: the room ID (integer)
        """
        for r_id in self.building_data.room_ids_list:
            if th_l_id in self.building_data.room(r_id).get_comfort_loads(EMS_COMFORT_temperature):
                return r_id

        return None

            ##########################
        ######### GUI FUNCTIONS #########
            ##########################

    def push_to_gui(self, msg, type_msg=None):
        if type_msg is None:
            self.gui_data.put(msg)
        else:
            self.gui_data.put(format_log_msg(GUI_LOG_SENDER_NAME_EMS, type_msg, msg))

    def send_gui_state(self):

        # The received state of the commanded entity correspond to the sensor data of the last time step
        self.update_ems_controlled_entities_state()

        if self.last_ems_step != self.current_time:  # Don't send data at the first iteration
            # Send the current_state
            payload = {EMS_ANALYZER.MSG_TYPE_KEY: EMS_ANALYZER.MSG_TYPE_ADD,
                       EMS_ANALYZER.MSG_CONTENT_DATA: self.current_state,
                       EMS_ANALYZER.MSG_CONTENT_TIME: self.last_ems_step}

            if SIMULATION_GUI_CONNECTED:
                self.push_to_gui(type_msg=GUI_LOG_TYPE_DATA, msg=payload)
            else:
                self.push_to_gui(payload)

        # Update the state
        self.update_building_state()

    def update_ems_controlled_entities_state(self):

        # The building entities that are controlled by the ems
        for (t, i) in self.current_command_sent:
            self.update_energy_entity_state(t, i)

        self.current_command_sent = []

        # The ems-level data that has been decided at the previous time step
        self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL][EMS_LEVEL_DATA_SOLD_ELEC] = self.current_selling_gen

        # The deferrable load that have been triggered, whose values are shifted by dt
        list_def_loads = self.building_data.get_entity_list([EMS_CATEGORY_ENTITY_LOAD,
                                                             EMS_CATEGORY_ENTITY_LOAD_DEFERRABLE])
        for (l_id, l_obj) in list_def_loads:
            if l_obj.is_running(self.last_ems_step):
                self.update_energy_entity_state(EMS_CATEGORY_ENTITY_LOAD, l_id)

    def update_energy_entity_state(self, e_type, e_id):

        obj = self.building_data.get_energy_entity_obj(e_type, e_id)

        if obj is None:
            return

        name = obj.name
        if e_type != EMS_CATEGORY_ENTITY_STORAGE:
            if obj.current_power is not None:
                self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][e_type][name] = obj.current_power
        else:
            if obj.current_power is not None:
                self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_power][name] = obj.current_power
                self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_soc][name] = obj.current_soc

    def update_building_state(self):
        """
        Return a structured dictionary describing the actual state of the building
        :return: a structured dictionary
        """
        self.current_state = EMS_ANALYZER.get_data_format()

        # Loads, generation and storage
        _loads = self.building_data.get_entity_list(EMS_CATEGORY_ENTITY_LOAD)
        _stor = self.building_data.get_entity_list(EMS_CATEGORY_ENTITY_STORAGE)
        _gen = self.building_data.get_entity_list(EMS_CATEGORY_ENTITY_GENERATION)

        #TODO:
        """ For the moment, a power value of 0 is given when the power is unknown. 
        This is due to the fact that the GUI doesn't compare the same 't' when plotting the grid consumption, 
        and so a shift appeared for the generated power """

        for (l_id, l_obj) in _loads:
            v = 0
            if l_obj.current_power is not None:
                v = l_obj.current_power
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_LOAD][l_obj.name] = v

        for (s_id, s_obj) in _stor:
            v = 0
            if s_obj.current_power is not None:
                v = s_obj.current_power
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_power][s_obj.name] = v

            v = 0
            if s_obj.current_soc is not None:
                v = s_obj.current_soc
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_soc][s_obj.name] = v

        for (g_id, g_obj) in _gen:
            v = 0
            if g_obj.current_power is not None:
                v = g_obj.current_power
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_GENERATION][g_obj.name] = v

            # Room comfort values & constraints
        for _r in self.building_data.room_list:
            room_name = _r.name
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT][room_name] = {}  # temp, etc
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS][room_name] = {}  # temp, etc
            self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL][room_name] = {}  # temp, etc

            list_of_comfort = BuildingRoomData.TYPE_COMFORT_LIST
            for type_comfort in list_of_comfort:

                # Value
                v = _r.get_comfort_value(type_comfort)
                if v is not None:
                    self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT][room_name][type_comfort] = v

                # Constraint
                c = _r.get_comfort_constraint(type_comfort)
                if c is not None:
                    self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS][room_name][type_comfort] = {}
                    self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS][room_name][type_comfort]['min'] = c.get_min(self.current_time)
                    self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS][room_name][type_comfort]['max'] = c.get_max(self.current_time)

                # Reference
                ref = _r.get_reference_value(type_comfort)
                if ref is not None:
                    self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL][room_name][type_comfort] = ref

        # Electricity price
        time_data = (self.current_time, self.current_time+self.ems_dt, self.ems_dt)
        price_vector = self.get_elec_price(time_data)
        self.current_state[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL][EMS_ENV_DATA_ELEC_PRICE] = price_vector[0]

        if EMS_VERBOSE:
            print("@time [{0}] state: {1}".format(self.current_time, self.current_state))
