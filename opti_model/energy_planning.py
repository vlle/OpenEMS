__author__ = 'Olivier Van Cutsem'


class EnergyPlanning(object):
    """
    This class represents a planning of the building energy
    """

    def __init__(self):

        self.__power_forecast = []

    @property
    def power_forecast(self):
        """
        Getter for __power_forecast
        :return:
        """
        return self.__power_forecast

    @power_forecast.setter
    def power_forecast(self, p_for):
        """
        Setter for __power_forecast
        :param p_for:
        :return:
        """
        self.__power_forecast = p_for
