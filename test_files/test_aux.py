from bmsinterface.bms_missing_data import BMS_MISSING_DATA_ENVIRONMENTAL_SIGNAL
from building_data_management.signal_and_constraint_management.signal_data import PiecewiseConstantSignal

t_cur = (3600*24*17)+8*3600
t_hor = 4*3600
t_step = 15*60
data_raw = BMS_MISSING_DATA_ENVIRONMENTAL_SIGNAL['price']

sig_object = PiecewiseConstantSignal(data_raw['t'], data_raw['v'])

print(sig_object.get_interval_val(t_cur, t_cur+t_hor, t_step))

