import auxiliary.requests.api as requests

__author__ = 'Olivier Van Cutsem'

OPENBMS_IP = '128.178.19.163'  #128.178.19.240
OPENBMS_PORT = '80'

RT_SERVER_IP = OPENBMS_IP
RT_SERVER_PORT = '8000'

# NEW ADDITIONAL PARAMETERS

data = "{test=2}"

# SEND THE REQUEST FOR THE MODIFICATION OF THE LOAD'S ADDITIONAL PARAMETERS
req_url = "http://{0}:{1}/API/{2}/{3}?app_sec_key=asdrrt12".format(OPENBMS_IP, OPENBMS_PORT, resource_type, resource_id, param)
r = requests.put(req_url, data)

# always check the return status code!
if r.status_code == 200:  # Otherwise it means openBMS doesnt work
    print("Modification successful")
elif r.status_code == 404 and r.content:  # Server replied but not found the midid
    print('Failed to find in openBMS the {0}: {1}'.format(resource_type, resource_id))
    sys.exit()
else:
    print('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
    sys.exit()
